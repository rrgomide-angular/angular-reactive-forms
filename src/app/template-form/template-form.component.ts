import { Component, OnInit } from '@angular/core';
import { NomeService } from '../services/nome.service';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {
  /**
   * Guarda o nome digitado
   * pelo usuário através de
   * two-way data-binding
   */
  nomeCompleto = '';

  /**
   * Indicativo de nome
   * validado
   */
  isNomeValido = false;

  /**
   * Vetor que guarda pendências
   * de validação
   */
  validacoes: string[] = [];

  /**
   * Vetor que guarda o cadastro de nomes
   */
  nomes: string[] = [];

  /**
   * O construtor recebe nomeService
   * através de injeção de dependência
   * @param nomeService Service de nomes
   */
  constructor(private nomeService: NomeService) {
    // Construtor vazio
  }

  /**
   * Já efetuamos uma validação
   * no início para dar um
   * feedback ao usuário
   */
  ngOnInit() {
    this.validarNome();
  }

  /**
   * Para validar o nome digitado,
   * invocamos o service que contém
   * toda a lógica, que pode então
   * ser reaproveitada
   */
  validarNome() {
    /**
     * O método nomeService.validarNome() retorna
     * um objeto
     */
    const validacao = this.nomeService.validarNome(this.nomeCompleto);

    this.validacoes = validacao.validacoes;
    this.isNomeValido = validacao.isNomeValido;
  }

  cadastrarNome(formulario) {
    if (!this.isNomeValido) {
      return;
    }

    /**
     * Cadastramos o nome inserindo o
     * valor no vetor
     */
    this.nomes.push(this.nomeCompleto);

    /**
     * Efetuamos o reset no form
     */
    formulario.reset();
  }
}
