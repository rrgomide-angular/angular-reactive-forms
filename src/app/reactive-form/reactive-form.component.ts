import { Component, OnInit } from '@angular/core';
import { NomeService } from '../services/nome.service';

import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {
  /**
   * O formulário é do tipo FormGroup,
   * que pertence a @angular/forms
   */
  reactiveForm: FormGroup;

  /**
   * Indicativo de nome
   * validado
   */
  isNomeValido = false;

  /**
   * Vetor que guarda pendências
   * de validação
   */
  validacoes: string[] = [];

  /**
   * Vetor que guarda o cadastro de nomes
   */
  nomes: string[] = [];

  /**
   *
   * Incluímos nomeService com DI, assim como
   * foi feito no template-form
   *
   */
  constructor(private nomeService: NomeService) {
    // Construtor vazio
  }

  ngOnInit() {
    /**
     * Declarando o formulário com FormGroup e
     * incluindo apenas um controle para o
     * nome
     */
    this.reactiveForm = new FormGroup({
      nomeCompleto: new FormControl('')
    });

    /**
     * Utilizando RxJS para monitorar alterações
     * no formulário e validar o nome digitado
     * pelo usuário
     */
    this.reactiveForm.valueChanges.subscribe(value => {
      this.validarNome(value.nomeCompleto);
    });

    /**
     * Forçando a validação no início
     * com o nome vazio
     */
    this.validarNome('');
  }

  /**
   * Para validar o nome digitado,
   * invocamos o service que contém
   * toda a lógica, que pode então
   * ser reaproveitada
   */
  validarNome(nomeToValidate) {
    /**
     * O método nomeService.validarNome() retorna
     * um objeto
     */
    const validacao = this.nomeService.validarNome(nomeToValidate);

    this.validacoes = validacao.validacoes;
    this.isNomeValido = validacao.isNomeValido;
  }

  cadastrarNome() {
    /**
     * Obtendo o valor digitado pelo usuário
     */
    const nomeCompleto = this.reactiveForm.get('nomeCompleto').value;

    /**
     * Certificando de que o valor é
     * realmente válido
     */
    if (!this.isNomeValido) {
      return;
    }

    /**
     * Cadastramos o nome inserindo o
     * valor no vetor
     */
    this.nomes.push(nomeCompleto);

    /**
     * Por fim, efetuamos o reset do form
     */
    this.reactiveForm.reset();
  }
}
