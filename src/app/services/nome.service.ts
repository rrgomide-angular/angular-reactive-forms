import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NomeService {
  minCaracteres = 10;

  constructor() {}

  validarNome(nome) {
    /**
     * Vetor de validações
     */
    const validacoes = [];

    /**
     * Assumimos que o nome
     * é válido no início
     */
    let isNomeValido = true;

    /**
     * O nome pode vir nulo.
     * Validamos isso aqui.
     * Além disso, limpamos
     * espaços em branco desnecessários
     * com trim(), se for o caso
     */
    const cleanedNome = !!nome ? nome.trim() : '';

    /**
     * Se encontrarmos uma sequência
     * de pelo menos 2 espaços em
     * branco, invalidamos o nome
     */
    if (cleanedNome.indexOf('  ') !== -1) {
      validacoes.push('Esse nome contem espaços em branco desnecessários.');
      isNomeValido = false;
    }

    /**
     * Validando quantidade de caracteres
     * mínima para o nome
     */
    if (cleanedNome.length < this.minCaracteres) {
      validacoes.push(
        this.minCaracteres - cleanedNome.length + ' caracter(es) restante(s).'
      );

      isNomeValido = false;
    }

    /**
     * Validando espaços em branco,
     * que indicam um sobrenome
     *
     */
    if (!cleanedNome.includes(' ')) {
      validacoes.push('O nome completo deve possuir pelo menos um sobrenome.');

      isNomeValido = false;
    }

    /**
     * Se entrar aqui o nome é válido
     *
     */
    if (isNomeValido) {
      validacoes.push('Nome apto a ser cadastrado!');
    }

    /**
     * Retornamos um objeto
     * com o flag de nome válido
     * e um vetor de validações
     */
    return {
      isNomeValido, // => isNomeValido: isNomeValido,
      validacoes // => validacoes: validacoes,
    };
  }
}
